<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'LoginController@showHome');
Route::get('/login', 'LoginController@showLogin');
Route::get('/register', 'UserController@showRegister');

Route::group(['middleware' => 'throttle:4,1'], function () {
    Route::get('/verify-otp', 'UserController@viewVerifyOTP');
    Route::post('/resend-otp', 'UserController@resendOTP');
    Route::post('/verify-otp', 'UserController@verifyOTP');
    Route::post('/forgot-password', 'LoginController@forgotPassword');
    Route::post('/forgot-password/resend-otp', 'LoginController@resendForgotPassword');
});

Route::post('/is-phone-registered', 'UserController@isPhoneRegistered');

Route::post('/is-email-registered', 'UserController@isEmailRegistered');

Route::get('/forgot-password', 'LoginController@showForgotPassword');

Route::get('/reset-password', 'LoginController@showResetPassword');

Route::post('/reset-password', 'LoginController@resetPassword');

// Only 5 times login allowed in a minute
Route::group(['middleware' => 'throttle:5,1'], function () {
    Route::post('/login', 'LoginController@login');
    Route::post('/register', 'UserController@register');
});

Route::middleware('checkLogin')->group(function () {
    Route::get('/new-request', 'RequestController@newRequest');

    Route::post('/get-category', 'FailureClassController@getFailureClass');

    Route::get('/list-request/placed-requests', 'RequestController@listPlacedRequest');

    Route::get('/list-request/update-requests', 'RequestController@listUpdateRequest');

    Route::get('/list-request/get-update-requests', 'RequestController@getUpdateRequest');

    Route::get('/profile-update', function () {
        return view('profile_update', ['userDetails' => session()->get('user_details')]);
    });
    Route::post('/profile-update', 'UserController@updateProfile');

    Route::get('/change-phone-no', function () {
        return view('change_phone_no', ['userDetails' => session()->get('user_details')]);
    });

    Route::post('/phone-update', 'UserController@updatePhone');

    Route::get('/change-password', function () {
        return view('change-password');
    });
    Route::post('/change-password', 'UserController@changePassword');

    Route::get('/logout', 'LoginController@logout');

    Route::post('/new_cust_request', 'RequestController@submitRequest');

    Route::get('/contact-us', 'LoginController@contactUs');

});

/*
 *
 */
