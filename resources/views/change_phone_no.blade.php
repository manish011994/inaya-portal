@extends('app')
@section('title') Inaya Portal - Change Phone no @endsection

@section('content')
<!-- Main Content -->
<div class="main-content container">
    <section class="section">
        <div class="section-header">
            <h1>Customer - Phone Update</h1>
        </div>

        <div class="section-body">

            <form id="phone_update" method="post" action="{{ URL::to('/phone-update') }}">
                {{ csrf_field() }}
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group offset-2 col-8 mb-0">
                                <label for="phone">Existing Phone</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group offset-2 col-1">
                                <input id="old_phone_ext" type="text" class="form-control form-control-sm"
                                       name="old_phone_ext" value="+971" readonly disabled>
                            </div>
                            <div class="form-group col-7">
                                <input id="phone" type="number" min="111111111" max="999999999" maxlength="10"
                                       class="form-control form-control-sm" name="phone"
                                       value="{{ $userDetails->phone }}" readonly disabled>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group offset-2 col-8 mb-0">
                                <label for="primary_phone">New Phone</label>
                            </div>
                            <div class="form-group offset-2 col-1">
                                <input id="new_phone_ext" type="text" class="form-control form-control-sm"
                                       name="new_phone_ext" value="+971" readonly disabled>
                            </div>
                            <div class="form-group col-7">
                                <input id="phone" type="number" min="111111111" max="999999999" maxlength="10"
                                       class="form-control form-control-sm" name="phone" onblur="$('#response_msg').html('').removeClass('alert-danger').removeClass('alert-success');"
                                       value="" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="offset-2 col-8 align-self-center">
                                <div class="alert" id="response_msg">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer bg-whitesmoke text-right">
                        <div class="row">
                            <div class="col-3"></div>
                            <div class="col-6 align-self-end">
                                <button class="btn btn-success mr-1" type="submit" id="update_phone"> Submit
                                </button>
                                <button class="btn btn-secondary" type="reset">Reset</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection
@section('js_content')
<script type="text/javascript">
    $(function () {
        $("#phone_update").submit(function (e) {
            $("#response_msg").removeClass('alert-danger').removeClass('alert-success');
            let $form = $(this);
            if ($("#phone_update").valid()) {
                var postData = $(this).serializeArray();
                var formURL = $(this).attr("action");
                $.ajax({
                    url: formURL,
                    type: "POST",
                    data: postData,
                    dataType: "json",
                    beforeSend: function () {
                        $("#update_phone").html("Please wait <i class='fa fa-spinner fa-spin'></i>").prop("disabled", "true");
                    },
                    success: function (response) {
                        if (response.result == 'success') {
                            $("#response_msg").addClass('alert-success').html(response.msg);
                        } else {
                            $("#response_msg").addClass('alert-danger').html(response.msg);
                        }
                    },
                    error: function () {
                        $("#response_msg").addClass('alert-danger').html("<p>Some error occurred at the Server.</p>");
                    },
                    complete: function () {
                        $("#update_phone").html("Submit").removeAttr("disabled");
                    }
                });
            }
            e.preventDefault();
        });
    });
</script>
@endsection
