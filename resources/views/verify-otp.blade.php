<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <link rel="icon" href="{{ asset('/images/inayaico.png') }}" sizes="32x32"/>
    <link rel="icon" href="{{ asset('/images/inayaico.png') }}" sizes="192x192"/>
    <link rel="apple-touch-icon" href="{{ asset('/images/inayaico.png') }}"/>
    <title>Verify OTP - Inaya Portal</title>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
          integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/components.css') }}">
</head>

<body>
<div id="app">
    <section class="section">
        <div class="container mt-2">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                    <div class="login-brand">
                        <img src="{{ URL::to('/images/INAYA.svg') }}" alt="logo" width="250"/>
                    </div>

                    <div class="card card-primary">
                        <div class="card-header"><h4>Phone Verification</h4></div>

                        <div class="card-body">
                            <form id="otp_form" method="POST" action="{{ URL::to('/verify-otp') }}"
                                  class="needs-validation" autocomplete="off" novalidate="">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="otp">We have sent an OTP on your phone number
                                        +971 {{ $phone }}</label>
                                    <input id="otp" type="number" class="form-control" name="otp" tabindex="1"
                                           placeholder="" autocomplete="off" required autofocus>
                                </div>
                                <div class="form-group text-right">
                                    Didn't received OTP?
                                    <button id="resend_otp" type="button" class="btn btn-sm btn-link">Resend OTP
                                    </button>
                                </div>
                                <div id="response_msg" class="alert mb-2"></div>
                                <div class="form-group mt-1">
                                    <button type="submit" id="submit_otp"
                                            class="btn btn-success btn-lg btn-block font-weight-bold font-size-20"
                                            tabindex="4">
                                        Verify
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="simple-footer">
                        Copyright &copy; {{ date('Y') }} Powered by <a
                            href="https://onlinemarketingdubai.ae">Redberries</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- General JS Scripts -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script src="{{ asset('/js/inaya.js') }}"></script>
<!-- JS Libraies -->
<script src="{{ asset('/js/jquery.validate.min.js') }}"></script>
<!-- Page Specific JS File -->

<!-- Template JS File -->
<script src="{{ URL::to('/js/scripts.js') }}"></script>
<script src="{{ URL::to('/js/custom.js') }}"></script>
<script type="text/javascript">
    $(function () {
        $("#otp_form").submit(function (e) {
            $("#response_msg").removeClass('alert-danger').removeClass('alert-success');
            if ($("#otp_form").valid()) {
                var postData = $(this).serializeArray();
                var formURL = $(this).attr("action");
                $.ajax({
                    url: formURL,
                    type: "POST",
                    data: postData,
                    dataType: "json",
                    beforeSend: function () {
                        $("#submit_otp").html("Please wait <i class='fa fa-spinner fa-spin'></i>").prop("disabled", "true");
                    },
                    success: function (response) {
                        if (response.success) {
                            $("#response_msg").addClass('alert-success').html(response.msg);
                            window.location.reload();
                        } else {
                            $("#response_msg").addClass('alert-danger').html(response.msg);
                        }
                    },
                    error: function () {
                        $("#response_msg").addClass('alert-danger').html("<p>Some error occurred at the Server.</p>");
                    }
                    ,
                    complete: function () {
                        $("#submit_otp").html("Verify").removeAttr("disabled");
                    }
                });
                e.preventDefault(); //STOP default action
            }
        });

        $("#resend_otp").click(function (e) {
            $("#otp").val("");
            $("#response_msg").removeClass('alert-danger').removeClass('alert-success');
            var formURL = $(this).attr("action");
            $.ajax({
                url: '{{ URL::to('/resend-otp') }}',
                type: "POST",
                data: {"_token": '{{ csrf_token() }}'},
                dataType: "json",
                beforeSend: function () {
                    $("#resend_otp").html("Please wait <i class='fa fa-spinner fa-spin'></i>").prop("disabled", "true");
                },
                success: function (response) {
                    if (response.success) {
                        $("#response_msg").addClass('alert-success').html(response.msg);
                    } else {
                        $("#response_msg").addClass('alert-danger').html(response.msg);
                    }
                },
                error: function () {
                    $("#response_msg").addClass('alert-danger').html("<p>Some error occurred at the Server.</p>");
                }
                ,
                complete: function () {
                    $("#resend_otp").html("Resend OTP").removeAttr("disabled");
                }
            });
        });
    });
</script>
</body>
</html>
