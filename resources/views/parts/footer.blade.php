<footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; {{ date('Y') }}
        <div class="bullet"></div>
        Powered by <a href="https://onlinemarketingdubai.ae">Redberries</a>
    </div>
    <div class="footer-right">
        1.0.0
    </div>
</footer>
