@extends('app')
@section('title') Inaya Portal - New Request @endsection

@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Customer - Change Password</h1>
            </div>

            <div class="section-body">
                <form id="change-password" method="post" action="{{ URL::to('/change-password') }}">
                    {{ csrf_field() }}
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="form-group">
                                        <label for="password">Current Password</label>
                                        <input id="password" name="password" type="password" autocomplete="off"
                                               class="form-control form-control-sm" value=""
                                               required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="form-group">
                                        <label for="new_password">New Password</label>
                                        <input id="new_password" name="new_password" type="password" autocomplete="off"
                                               class="form-control form-control-sm" value="" minlength="8" maxlength="30"
                                               required/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="form-group">
                                        <label for="confirm_password">Confirm New Password</label>
                                        <input id="confirm_password" name="confirm_password"
                                               type="password" class="form-control form-control-sm" autocomplete="off"
                                               value="" minlength="8" maxlength="30" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-center">
                                    <div class="alert" id="response_msg">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-whitesmoke text-right">
                            <div class="row">
                                <div class="col-3"></div>
                                <div class="col-6 align-self-end">
                                    <button class="btn btn-success mr-1" type="submit" id="change-password-btn"> Change
                                        Password
                                    </button>
                                    <button class="btn btn-secondary" type="reset">Reset</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection
@section('js_content')
    <script type="text/javascript">
        $(function () {
            $("#change-password").submit(function (e) {
                $("#response_msg").removeClass('alert-danger').removeClass('alert-success');
                var pwd = $("#new_password").val();
                var pwd1 = $("#confirm_password").val();
                if (pwd !== pwd1) {
                    $("#response_msg").addClass('alert-danger').html("<p>Passwords not matched.</p>");
                    return false;
                }
                let $form = $(this);
                if ($("#change-password").valid()) {
                    var postData = $(this).serializeArray();
                    var formURL = $(this).attr("action");
                    $.ajax({
                        url: formURL,
                        type: "POST",
                        data: postData,
                        dataType: "json",
                        beforeSend: function () {
                            $("#change-password-btn").html("Please wait <i class='fa fa-spinner fa-spin'></i>").prop("disabled", "true");
                        },
                        success: function (response) {
                            if (response.success) {
                                $form[0].reset();
                                $("#response_msg").addClass('alert-success').html(response.msg);
                            } else {
                                $("#response_msg").addClass('alert-danger').html(response.msg);
                            }
                        },
                        error: function () {
                            $("#response_msg").addClass('alert-danger').html("<p>Some error occurred at the Server.</p>");
                        }
                        ,
                        complete: function () {
                            $("#change-password-btn").html("Change Password").removeAttr("disabled");
                        }
                    });
                    e.preventDefault();
                }
            });
        });
    </script>
@endsection
