<html>
<body>
<p>Dear {{ $phoneUpdateRequest['primary_name'] }},</p>
<p>
    We have received your phone number change request from {{ $phoneUpdateRequest['old_phone'] }} to
    {{ $phoneUpdateRequest['new_phone'] }}. <br/>
    We will update you once the update is completed.
</p>
</body>
</html>
