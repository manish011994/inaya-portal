<html>
<body>
<p>Dear Admin,</p>
<p>
    User {{ $phoneUpdateRequest['primary_name'] }} has placed a phone number change request.<br/>
    Please visit the staff portal to approve the changes.<br/>
    <br/>
    Request Details<br/>
    Old phone number : {{ $phoneUpdateRequest['old_phone'] }}<br/>
    New phone number: {{ $phoneUpdateRequest['new_phone'] }}<br/>
    Primary Name : {{ $phoneUpdateRequest['primary_name'] }}<br/>
    Primary email address: {{ $phoneUpdateRequest['primary_email'] }}<br/>
    Secondary name: {{ $phoneUpdateRequest['secondary_name'] }}<br/>
    Secondary email: {{ $phoneUpdateRequest['secondary_email'] }}<br/>
    Secondary phone: {{ $phoneUpdateRequest['secondary_phone'] }}<br/>
    Unit ID : {{ $phoneUpdateRequest['unit_id'] }}<br/>
</p>
</body>
</html>
