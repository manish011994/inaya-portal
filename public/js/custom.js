"use strict";

$(function () {

    $("#work_order_category").change(function () {
        var val = $(this).val();
        if (val == "CIVIL") {
            $("#failure_class").html("<option value='CARPENTRY'>CARPENTRY</option><option value='MASONRY'>MASONRY</option><option value='PAINTING'>PAINTING</option>");
        } else if (val == "MEP") {
            $("#failure_class").html("<option value='HVAC'>HVAC</option>" +
                "<option value='FIRE ALARM'>FIRE ALARM</option>" +
                "<option value='FIRE PROTECTION'>FIRE PROTECTION</option>" +
                "<option value='PA SYSTEM'>PA SYSTEM</option>" +
                "<option value='ELEVATORS'>ELEVATORS</option>" +
                "<option value='ESCALATORS'>ESCALATORS</option>" +
                "<option value='MECHANICAL'>MECHANICAL</option>" +
                "<option value='PLUMBING'>PLUMBING</option>" +
                "<option value='GAS SYSTEM'>GAS SYSTEM</option>" +
                "<option value='POOLS & WATER FEATURES'>POOLS & WATER FEATURES</option>" +
                "<option value='CCTV SYSTEM'>CCTV SYSTEM</option>" +
                "<option value='BMS'>BMS</option>" +
                "<option value='ELECTRICAL'>ELECTRICAL</option>" +
                "<option value='ELECTRONICS'>ELECTRONICS</option>" +
                "<option value='CCTV'>CCTV</option>");
        } else if (val == "REFURBISHMENT") {
            $("#failure_class").html("<option value='REFURBISHMENT'>REFURBISHMENT</option>");
        } else if (val == "SOFT SERVICES") {
            $("#failure_class").html("<option value='CLEANING'>CLEANING</option>" +
                "<option value='WASTE MANAGEMENT'>WASTE MANAGEMENT</option>" +
                "<option value='PEST CONTROL'>PEST CONTROL</option>");
        } else if (val == "SPECIAL SYSTEMS") {
            $("#failure_class").html("<option value='CRADLE'>CRADLE</option>" +
                "<option value='HAS'>HAS</option>" +
                "<option value='HOME APPLIANCES'>HOME APPLIANCES</option>" +
                "<option value='SLIDING DOORS'>SLIDING DOORS</option>" +
                "<option value='GYMNASIUM'>GYMNASIUM</option>");
        }
    });

    $("#new_request").submit(function (e) {
        let $form = $(this);
        let action = $form.attr('action');
        let formData = $form.serialize();

        $.ajax({
            type: 'POST', // define the type of HTTP verb we want to use (POST for our form)
            url: action, // the url where we want to POST
            data: formData, // our data object
            dataType: 'json', // what type of data do we expect back from the server
            beforeSend: function () {
                $("#response_msg").removeClass("alert-success").removeClass("alert-danger").html("");
                $("#register").html("<i class=\"fa fa-spinner fa-spin\"></i> &nbsp; Please wait");
            },
        })
            .done(function (data) {
                if (data.result == "success") {
                    $form[0].reset();
                    $("#response_msg").addClass("alert-success").html("Your request placed successfully.");
                } else if (data.result == "failed") {
                    $("#response_msg").addClass("alert-danger").html(data.message);
                } else {
                    $("#response_msg").addClass("alert-danger").html("Something went wrong the submitted request.");
                }
            })
            .fail(function () {
                alert("Some Error Occurred on the Server.");
            })
            .always(function () {
                $("#register").html("Submit");
            });
        e.preventDefault();
    });

    $("#job_status").change(function () {
        console.log($(this).val());
        if ($(this).val() != "") {
            $(".data-row").hide();
            $("." + $(this).val().replace(" ", "_")).show();
        } else {
            $(".data-row").show();
        }
    });
});
