<?php

namespace App\Console\Commands;

use App\Models\UserRequestsFilesMetaDataModel;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\URL;
use phpseclib\Net\SFTP;

class FetchUserUpdateRequest extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user_update_request';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetches the Customer Update Request from SFTP Server';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        ini_set('max_execution_time', 0);
        //Starting the SFTP Transfer
        $fileLists = array();
        $sftp = new SFTP(env('SFTP_HOST'));
        $currentUrl = URL::current();
        $sftp_login = $sftp->login(env('SFTP_USERNAME'), env('SFTP_PASSWORD'));

        if ($sftp_login) {
            createLog('custupdaterequestcron@inaya.ae', $currentUrl, 'SFTP Logged In Success', $sftp, $sftp_login);
            $sftp->chdir(env('CUST_CALL_UPDATE_REQUEST'));
            $files = $sftp->rawlist();
            $this->info("Files Loaded");
            foreach ($files as $file) {
                if (!in_array($file['filename'], ['.', '..']) &&
                    strtolower(pathinfo($file['filename'])['extension']) == 'csv') { //Exclude These from the File list
                    $userUpdateRequestMetaModel = UserRequestsFilesMetaDataModel::getFileDetails($file);
                    $fileLists[] = $file;
                    if ($userUpdateRequestMetaModel == 'download') {
                        //File Not Present
                        // Add the File in MetaData Table
                        $userUpdateRequestData = new UserRequestsFilesMetaDataModel();
                        $userUpdateRequestData->file_name = $file['filename'];
                        $userUpdateRequestData->file_size = $file['size'];
                        $userUpdateRequestData->file_created = $file['atime'];
                        $userUpdateRequestData->last_modified = $file['mtime'];
                        if ($userUpdateRequestData->save()) {
                            $this->info("Meta Data Saved for - " . $userUpdateRequestData->file_name);
                            $sftp->ping();
//                            $storeFile = $sftp->get(env('CUST_CALL_UPDATE_REQUEST') . $file['filename'], env('DOWNLOAD_CUST_UPDATE_REQUEST_PATH') . $file['filename']);
                            $size = $sftp->size(env('CUST_CALL_UPDATE_REQUEST') . $file['filename']);
                            for ($offset = 0; $offset < $size; $offset += 1024 * 1024) {
                                $sftp->get(env('CUST_CALL_UPDATE_REQUEST') . $file['filename'], env('DOWNLOAD_CUST_UPDATE_REQUEST_PATH') . $file['filename'], $offset, 1024 * 1024);
                            }
                            $sftp->ping();
                        } else {
                            createLog('custupdaterequestcron@inaya.ae', $currentUrl, 'File Meta Data Saving Failed for File - ' . $file['filename'], $userUpdateRequestData, "Error in saving");
                            // TODO Send Email
                        }
                    } else if ($userUpdateRequestMetaModel == 'update_download') {
                        //File Present
                        // Update the File in MetaData Table
                        $userUpdateRequestData = UserRequestsFilesMetaDataModel::where('file_created', $file['atime'])->first();
                        $userUpdateRequestData->file_name = $file['filename'];
                        $userUpdateRequestData->file_size = $file['size'];
                        $userUpdateRequestData->file_created = $file['atime'];
                        $userUpdateRequestData->last_modified = $file['mtime'];
                        if ($userUpdateRequestData->save()) {
                            $this->info("Meta Data Updated for - " . $userUpdateRequestData->file_name);
                            $sftp->ping();
//                            $storeFile = $sftp->get(env('CUST_CALL_UPDATE_REQUEST') . $file['filename'], env('DOWNLOAD_CUST_UPDATE_REQUEST_PATH') . $file['filename']);
                            $size = $sftp->size(env('CUST_CALL_UPDATE_REQUEST') . $file['filename']);
                            for ($offset = 0; $offset < $size; $offset += 1024 * 1024) {
                                $sftp->get(env('CUST_CALL_UPDATE_REQUEST') . $file['filename'], env('DOWNLOAD_CUST_UPDATE_REQUEST_PATH') . $file['filename'], $offset, 1024 * 1024);
                            }
                            $sftp->ping();
                        } else {
                            createLog('custupdaterequestcron@inaya.ae', $currentUrl, 'File Meta Data Saving Failed for File - ' . $file['filename'], $userUpdateRequestData, "Error in saving");
                            // TODO Send Email
                        }
                    }
                }
            }
            $sftp->disconnect();
            //Store Data in Unit Master Table
            storeUserUpdateRequestDataFromCSV($fileLists, $this);
            $this->info("Process Finished");
        } else {
            $error = $sftp->getLastSFTPError();
            $result['result'] = 'failed';
            $result['message'] = getSFTPError($error);
            createLog('custupdaterequestcron@inaya.ae', $currentUrl, 'SFTP Login Failed', $sftp, $sftp_login);
        }
    }
}
