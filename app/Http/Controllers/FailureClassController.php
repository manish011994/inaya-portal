<?php

namespace App\Http\Controllers;

use App\Models\CategoryModel;
use Illuminate\Http\Request;

class FailureClassController extends Controller
{
    public function manageFailureClass(Request $request, $categoryId)
    {
        $categoryName = CategoryModel::where('id', $categoryId)->where('status', 'Y')->first();
        if (!empty($categoryName)) {
            $failureClasses = CategoryModel::where('parent_id', $categoryId)
                ->where('status', 'Y')->get();
            return view('category_failure_class.manage_failure_class', ['categoryId' => $categoryId, 'categoryName' => $categoryName->category, 'failureClasses' => $failureClasses]);
        } else {
            return abort(404);
        }
    }

    public function addFailureClassPage($categoryId)
    {
        $categoryName = CategoryModel::where('id', $categoryId)->where('status', 'Y')->first();
        if (!empty($categoryName)) {
            return view('category_failure_class.add_failure_class', ['categoryId' => $categoryId, 'categoryName' => $categoryName->category]);
        } else {
            return abort(404);
        }
    }

    public function addFailureClass(Request $request, $categoryId)
    {
        $result = array();
        $failureClassName = $request->get('category');
        $isFailureClassAdded = CategoryModel::getFailureClassByName($failureClassName, $categoryId);
        if (empty($isFailureClassAdded->category)) {
            $failureClass = new CategoryModel();
            $failureClass->parent_id = $categoryId;
            foreach ($request->all() as $key => $value) {
                if (!in_array($key, ['_token'])) {
                    $failureClass->$key = $value;
                }
            }
            if ($failureClass->save()) {
                $result['success'] = true;
                $result['msg'] = $failureClassName . " added.";
            } else {
                $result['success'] = false;
                $result['msg'] = "Error in saving FailureClass.";
            }
        } else {
            $result['success'] = false;
            $result['msg'] = $failureClassName . " is already added.";
        }
        return json_encode($result);
    }

    public function showFailureClass($categoryId, $failureClassId)
    {
        $categoryName = CategoryModel::where('id', $categoryId)->first();
        $failureClass = CategoryModel::where('id', $failureClassId)->first();
        if (!empty($failureClass)) {
            return view('category_failure_class.add_failure_class', ['categoryName' => $categoryName->category, 'categoryId' => $categoryId, 'failureClass' => $failureClass]);
        } else {
            return abort(404);
        }
    }

    public function updateFailureClass(Request $request, $categoryId, $failureClassId)
    {
        $result = array();
        $failureClass = CategoryModel::where('id', $failureClassId)->where('parent_id', $categoryId)->where('status', 'Y')->first();
        if (!empty($failureClass)) {
            $failureClass->category = $request->get('category');
            if ($failureClass->save()) {
                $result['success'] = true;
                $result['msg'] = $failureClass->category . " updated.";
            } else {
                $result['success'] = false;
                $result['msg'] = "Error in updating.";
            }
            return json_encode($result);
        } else {
            return abort(404);
        }
    }

    public function deleteFailureClass($categoryId, $failureClassId)
    {
        $failureClass = CategoryModel::where('id', $failureClassId)->first();
        if (!empty($failureClass)) {
            $failureClass->status = 'N';
            if ($failureClass->save()) {
                return redirect('/category-failure-class/failure-class/' . $categoryId);
            }
        } else {
            return abort(404);
        }
    }

    public function getFailureClass(Request $request)
    {
        $failureClasses = [];
        if (!empty($request->post('category'))) {
            $category = CategoryModel::getCategoryByName($request->post('category'));
            $failureClasses = CategoryModel::select('category')
                ->where('parent_id', $category->id)
                ->where('status', 'Y')
                ->get();
        }
        return json_encode($failureClasses);
    }
}
