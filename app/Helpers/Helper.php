<?php

use App\Models\UnitMasterModel;
use App\Models\UserRequestLog;
use App\Models\UserUpdateRequestModel;
use GuzzleHttp\Client;
use League\Csv\Reader;
use League\Csv\Writer;


/**
 * @param $columnNames
 * @param $rows
 * @param string $fileName
 * Creating new CSV as per requirement
 */
function createCSV($columnNames, $rows, $fileName = 'file.csv')
{
    $csvFile = fopen($fileName, "w") or die("Unable to open file");
    fwrite($csvFile, implode(",", $columnNames));
    fwrite($csvFile, "\n");
    fwrite($csvFile, implode(",", $rows));
    fclose($csvFile);
    return true;
}

/*function createCSV($columnNames, $rows, $fileName = 'file.csv')
{
    try {
        $rows = array($rows); // For making iterable
        $writer = Writer::createFromPath($fileName, 'w+');
        $writer->insertOne((array)$columnNames);
        $writer->insertAll(new ArrayIterator($rows)); //using a Traversable object
        return $writer;
    } catch (League\Csv\Exception $exception) {
        return $exception->getMessage();
    }
}*/

function createLog($email, $url, $processName, $request, $response)
{
    $log = new UserRequestLog();
    $log->email = $email;
    $log->url = $url;
    $log->process_name = $processName;
    $log->request = json_encode($request);
    $log->response = json_encode($response);
    $log->save();
}

function getSFTPError($error)
{
    if (!empty($error) && str_contains($error, ":")) {
        return trim(explode(":", $error)[1]);
    } else {
        return "Something went wrong with SFTP Server Connection";
    }
}

function verifyCaptcha($request)
{
    $endpoint = "https://www.google.com/recaptcha/api/siteverify";
    $client = new Client();
    $response = $client->request('POST', $endpoint, ['query' => [
        'secret' => getenv('GOOGLE_RECAPTCHA_SECRET_KEY'),
        'response' => $request->get('g-recaptcha-response'),
        'remoteip' => $request->ip()
    ]]);
    $content = json_decode($response->getBody(), true);
    return $content['success'];
}

function sendSMS($phone, $message)
{
    $mobilenumbers = "971" . $phone;
    $endpoint = "http://www.smscountry.com/SMSCwebservice_Bulk.aspx";
    $client = new Client();
    $response = $client->request('POST', $endpoint, ['query' => [
        'User' => env('SMS_USERNAME'),
        'passwd' => env('SMS_PASSWORD'),
        'mobilenumber' => $mobilenumbers,
        'message' => $message,
        'sid' => env('SMS_SENDER_ID'),
        'mtype' => 'N',
        'DR' => 'Y'
    ]]);
    $content = json_decode($response->getBody(), true);
    return $content;
}

if (!function_exists('r_print')) {
    function r_print($data)
    {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }
}

function removeSpecialCharacters($data)
{
    foreach ($data as $key=>$value) {
        if(stripos($key, "email") === FALSE) {
            $data[$key] = str_replace(array('“', ';', '<', '>', '#', '!', '$', '%', '^', '&', '*', '(', ')', '}',
                                    '{', '[', ']', '?', '/', ',', '\\', '|', '+', '.', '"', "'", "_", ":"), "-", $value);
        }
    }
    return $data;
}
