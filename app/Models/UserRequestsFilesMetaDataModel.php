<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRequestsFilesMetaDataModel extends Model
{
    protected $table = 'user_requests_files_metadata';

    public static function getFileDetails($file)
    {
        $result = '';
        $userUpdateRequestFilePresent = UserRequestsFilesMetaDataModel::where('file_created', $file['atime'])
            ->where('file_size', $file['size'])
            ->where('last_modified', $file['mtime'])
            ->first();

        if(empty($userUpdateRequestFilePresent)) {
            $result = 'download';
        } else {
            //Check localfile size
            $localFile = env('DOWNLOAD_CUST_UPDATE_REQUEST_PATH') . $file['filename'];
            if ($userUpdateRequestFilePresent->file_size != @filesize($localFile)) {
                $result = 'update_download';
            }
        }
        return $result;
    }
}
