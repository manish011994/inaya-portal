<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserUpdateRequestModel extends Model
{
    protected $table = 'user_update_requests';

    public static function isComplaintPresent($complaintNo, $createdDate)
    {
        return UserUpdateRequestModel::where('complaint_no', $complaintNo)->where('created_date', $createdDate)->first();
    }

    public static function getUnitId($phone)
    {
        $data = UserUpdateRequestModel::select('unit_id')
            ->where('customer_phone', $phone)
            ->orderBy('created_at', 'DESC')
            ->first();
        if(!empty($data)) {
            return $data->unit_id;
        } else {
            return "INPX-XXX-XXX-XXX-XXX-XXXX-C";
        }
    }
}
