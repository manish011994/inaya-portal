<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserRequestLog extends Model
{
    protected $table = 'user_requests_logs';
}
