<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UnitMasterFilesMetaDataModel extends Model
{
    protected $table = 'unit_master_files_metadata';

    public static function getFileDetails($file)
    {
        $result = '';
        $unitMasterFilePresent = UnitMasterFilesMetaDataModel::where('file_created', $file['atime'])
            ->where('file_size', $file['size'])
            ->where('last_modified', $file['mtime'])
            ->first();

        if (empty($unitMasterFilePresent)) {
            $result = 'download';
        } else {
            //Check localfile size
            $localFile = env('DOWNLOAD_UNIT_MASTER_PATH') . $file['filename'];
            if ($unitMasterFilePresent->file_size != @filesize($localFile)) {
                $result = 'update_download';
            }
        }
        return $result;
    }
}
